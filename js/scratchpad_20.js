// const response = fetch('https://pokeapi.co/api/v2/pokemon');

// fetch('https://pokeapi.co/api/v2/pokemon')
// 	.then((res) => {
// 		return res.json();
// 	})
// 	.then((data) => {
// 		console.log(data);
// 	})
// 	.catch((err) => {
// 		console.log(err);
// 	});

// fetch('https://pokeapi.co/api/v2/pokeasdfasdfasdfklajsdhfmon')
// 	.then((res) => {
// 		// console.log('IN THE THEN BLOCK');
// 		// console.log(res);
// 		if (!res.ok) {
// 			throw new Error();
// 		}
// 		console.log(res);
// 	})
// 	.catch((err) => {
// 		console.log('IN THE CATCH BLOCK');
// 		console.log(err);
// 	});

const root = document.querySelector('#root');
root.style.display = 'grid';
root.style.gridTemplateColumns = '1fr 1fr 1fr 1fr';
root.style.gap = '40px';

axios
	.get('https://pokeapi.co/api/v2/pokemon?limit=100000')
	.then((res) => {
		for (let pokemon of res.data.results) {
			axios.get(pokemon.url).then((poke) => {
				const pokemonInfo = poke.data;

				const card = document.createElement('div');

				const img = document.createElement('img');
				img.src = pokemonInfo.sprites.other['official-artwork'].front_default;
				img.style.width = '100%';

				const title = document.createElement('h3');
				title.innerText = pokemonInfo.name;
				title.style.textAlign = 'center';

				card.append(img, title);
				root.append(card);
			});
		}
	})
	.catch((err) => {
		console.log('IN THE ERROR BLOCK');
		console.log(err);
	});
