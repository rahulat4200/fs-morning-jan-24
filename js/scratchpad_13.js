// function add(a = 0, b = 0) {
// 	return a + b;
// }

// // console.log(add(10, 20));

// const nums = [1, 2];

// console.log(add(...nums));

// const nums = [1123, 12, 334, 34, 566, 57, 4, 3, 2];

// console.log(Math.max(...nums));

// function dummy(a, b, c) {
// 	console.log(a);
// 	console.log(b);
// 	console.log(c);
// }

// const users = ['John', 'Jane', 'Jack'];

// dummy(...'hello');

// function printNames(a, b, ...names) {
// 	console.log(a);
// 	console.log(b);
// 	console.log(names);
// }

// printNames('John', 'jane', 'jack', 'hello', 'world');

// function add(...nums) {
// 	let total = 0;
// 	for (let num of nums) {
// 		total += num;
// 	}
// 	return total;
// }

// console.log(add(1, 2, 3, 4, 5, 6, 7, 8, 9));

// const users = ['john', 'jane', 'jack'];

// // const admin = users[0];
// // const mod = users[1];
// // const sub = users[2];

// const [admin, ...others] = users;

// console.log(admin, others);

const user = {
	firstName: 'John',
	lastName: 'Doe',
	email: 'john.doe@gmail.com',
	phone: 99982234567,
};

const { firstName, lastName, email, phone } = user;

// const firstName = user.firstName;
// const lastName = user.lastName;
// const email = user.email;
// const phone = user.phone;

// console.log(firstName, lastName, others);

function profile({ firstName, lastName, age, company }) {
	console.log(
		`Hello, my name is ${firstName} ${lastName} and I am ${age} years old. I work as a dev at ${company}.`
	);
}

// profile({ firstName: 'Jane', lastName: 'Doe', age: 30, company: 'AirBnb' });

// function add(a: number, b: number): number {
// 	return a + b;
// }

// add(10);

// /**
//  * @type {number}
//  */
// let hello;

// console.log(hello);
