class User:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def greet(self):
        print(
            f"Hello my name is {self.first_name} {self.last_name} and I am {self.age} years old"
        )


user1 = User("John", "Doe", 20)
user1.greet()
print(user1.age)
