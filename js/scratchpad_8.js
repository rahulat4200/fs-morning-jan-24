// // let nums = [1, 2, 34, 4, 5, 456, 5, 232];

// // for (let i = 0; i < nums.length; i++) {
// // 	console.log(nums[i]);
// // }

// // for (let num of nums) {
// // 	console.log(num);
// // }

// // for (let char of 'hello world') {
// // 	console.log(char);
// // }

// const matrix = [
// 	[1, 4, 7],
// 	[9, 7, 2],
// 	[9, 4, 6],
// ];

// for (let row of matrix) {
// 	for (let col of row) {
// 		console.log(col);
// 	}
// }

// const cats = ['fashion', 'mobiles', 'books'];
// const prods = ['tshirt', 'samsung', '1984'];

// for (let i = 0; i < cats.length; i++) {
// 	console.log(cats[i]);
// 	console.log(prods[i]);
// }

const productPrices = {
	Apple: 80000,
	OnePlus: 50000,
	Samsung: 90000,
};

// for (let value of Object.values(productPrices)) {
// 	console.log(value);
// }

// for (let key of Object.keys(productPrices)) {
// 	console.log(key, productPrices[key]);
// }

for (let key in productPrices) {
	console.log(key, productPrices[key]);
}
