// const willGetAPlayStation = new Promise((resolve, reject) => {
// 	const randomNum = Math.random();

// 	if (randomNum > 0.5) {
// 		resolve();
// 	} else {
// 		reject();
// 	}
// });

// willGetAPlayStation
// 	.then(() => {
// 		console.log('Promise was fulfilled');
// 	})
// 	.catch(() => {
// 		console.error('Promise failed');
// 	});

// function makePlayStationPromise() {
// 	return new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 			const rand = Math.random();
// 			if (rand < 0.5) {
// 				resolve();
// 			} else {
// 				reject();
// 			}
// 		}, 5000);
// 	});
// }

// makePlayStationPromise()
// 	.then(() => {
// 		console.log('PROMISE WAS FULFILLED');
// 	})
// 	.catch(() => {
// 		console.error('PROMISE WAS REJECTED');
// 	});

// console.log('THIS IS THE VERY LAST THING');

function fakeRequest(url) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			const pages = {
				'/users': [
					{ id: 1, username: 'john' },
					{ id: 2, username: 'jane' },
				],
				'/about': 'This is the about page',
				'/users/1': {
					id: 1,
					username: 'johndoe',
					topPostId: 53231,
					city: 'mumbai',
				},
				'/users/5': {
					id: 1,
					username: 'janedoe',
					topPostId: 32443,
					city: 'pune',
				},
				'/posts/53231': {
					id: 1,
					title: 'Really amazing post',
					slug: 'really-amazing-post',
				},
			};

			const data = pages[url];

			if (data) {
				resolve({ status: 200, data });
			} else {
				reject({ status: 404 });
			}
		}, 1000);
	});
}

// fakeRequest('/users')
// 	.then((res) => {
// 		console.log('First request was resolved');
// 		const userId = res.data[0].id;
// 		fakeRequest(`/users/${userId}`)
// 			.then((res) => {
// 				console.log('Second request was resolved');
// 				const postId = res.data.topPostId;
// 				fakeRequest(`/posts/${postId}`)
// 					.then((res) => {
// 						console.log(res);
// 					})
// 					.catch((err) => {
// 						console.log(err);
// 					});
// 			})
// 			.catch((err) => {
// 				console.log(err);
// 			});
// 	})
// 	.catch((err) => {
// 		console.error(err);
// 	});

fakeRequest('/users')
	.then((res) => {
		const userId = res.data[0].id;
		return fakeRequest(`/users/${userId}`);
	})
	.then((res) => {
		const postId = res.data.topPostId;
		return fakeRequest(`/posts/${postId}`);
	})
	.then((res) => {
		console.log(res);
	})
	.catch((err) => {
		console.error(err);
	});
