// // function initializeDeck() {
// // 	const deck = [];
// // 	const suits = ['hearts', 'diamonds', 'spades', 'clubs'];
// // 	const values = '2,3,4,5,6,7,8,9,10,J,Q,K,A';
// // 	for (let value of values.split(',')) {
// // 		for (let suit of suits) {
// // 			deck.push({ value, suit });
// // 		}
// // 	}
// // 	return deck;
// // }

// // function drawCard(deck, drawnCards) {
// // 	const card = deck.pop();
// // 	drawnCards.push(card);
// // 	return card;
// // }

// // function drawMultiple(numCards, deck, drawnCards) {
// // 	const cards = [];
// // 	for (let i = 0; i < numCards; i++) {
// // 		cards.push(drawCard(deck, drawnCards));
// // 	}
// // 	return cards;
// // }

// // function shuffle(deck) {
// // 	// loop over array backwards
// // 	for (let i = deck.length - 1; i > 0; i--) {
// // 		// pick random index before current element
// // 		let j = Math.floor(Math.random() * (i + 1));
// // 		[deck[i], deck[j]] = [deck[j], deck[i]];
// // 	}
// // 	return deck;
// // }

// // const deck1 = initializeDeck();
// // const hand = [];

// // shuffle(deck1);
// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawCard(deck1, hand);
// // drawMultiple(3, deck1, hand);

// // console.log(deck1);
// // console.log(hand);

// function makeDeck() {
// 	return {
// 		deck: [],
// 		drawnCards: [],
// 		suits: ['hearts', 'diamonds', 'spades', 'clubs'],
// 		values: '2,3,4,5,6,7,8,9,10,J,Q,K,A',
// 		initializeDeck() {
// 			for (let value of this.values.split(',')) {
// 				for (let suit of this.suits) {
// 					this.deck.push({ value, suit });
// 				}
// 			}
// 			return this.deck;
// 		},
// 		drawCard() {
// 			const card = this.deck.pop();
// 			this.drawnCards.push(card);
// 			return card;
// 		},
// 		drawMultiple(numCards) {
// 			const cards = [];
// 			for (let i = 0; i < numCards; i++) {
// 				cards.push(this.drawCard());
// 			}
// 			return cards;
// 		},
// 		shuffle() {
// 			const { deck } = this;
// 			for (let i = deck.length - 1; i > 0; i--) {
// 				let j = Math.floor(Math.random() * (i + 1));
// 				[deck[i], deck[j]] = [deck[j], deck[i]];
// 			}
// 		},
// 	};
// }

// // ENCAPSULATION
// // ABSTRACTION

// const deck1 = makeDeck();
// const deck2 = makeDeck();
// // deck1.initializeDeck();
// // deck1.shuffle();
// // deck1.drawCard();
// console.log(deck1);
// console.log(deck2);

// function user(firstName, lastName, age) {
// 	return {
// 		firstName,
// 		lastName,
// 		age,
// 	};
// }

// function User(firstName, lastName, age) {
// 	this.firstName = firstName;
// 	this.lastName = lastName;
// 	this.age = age;
// }

// User.prototype.greet = function () {
// 	return `Hello, my name is ${this.firstName} ${this.lastName}`;
// };

// const user0 = user('Jane', 'Doe', 20);

// const user1 = {
// 	firstName: 'John',
// 	lastName: 'Doe',
// 	age: 20,
// };

// const user2 = new User('Jack', 'Roe', 30);

// console.log(user0);
// console.log(user1);
// console.log(user2);
// console.log(user2.greet());

// function User(firstName, lastName, age) {
// 	this.firstName = firstName;
// 	this.lastName = lastName;
// 	this.age = age;
// }

// User.prototype.greet = function () {
// 	return `Hello, my name is ${this.firstName} ${this.lastName}`;
// };

class User {
	constructor(firstName, lastName, age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	greet() {
		return `Hello, my name is ${this.firstName} ${this.lastName}`;
	}

	login() {
		return `${this.firstName} has logged in`;
	}

	logout() {
		return `${this.firstName} has logged out`;
	}
}

class Admin extends User {
	constructor(firstName, lastName, age, phone) {
		super(firstName, lastName, age);
		this.phone = phone;
	}

	createGroup(groupName) {
		return `${groupName} was created by ${this.firstName}`;
	}
}

const user2 = new User('Jack', 'Roe', 30);
const user3 = new Admin('Jane', 'Doe', 24, '332432434324');

console.log(user3);
console.log(user3.createGroup('JavaScript'));
