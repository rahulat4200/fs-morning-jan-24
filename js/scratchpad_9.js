// // function greet() {
// // 	console.log('Hello World');
// // 	console.log('Goodbye World');
// // 	console.log('Test message');
// // }

// // for (let i = 0; i < 4; i++) {
// // 	greet();
// // }

// // console.log('Hello World');
// // console.log('Goodbye World');
// // console.log('Test message');

// // console.log('Hello World');
// // console.log('Goodbye World');
// // console.log('Test message');

// // console.log('Hello World');
// // console.log('Goodbye World');
// // console.log('Test message');

// // console.log('Hello World');
// // console.log('Goodbye World');
// // console.log('Test message');

// function flipCoin() {
// 	const randomNum = Math.random();
// 	if (randomNum > 0.5) {
// 		console.log('HEADS');
// 	} else {
// 		console.log('TAILS');
// 	}
// }

// flipCoin();

// function rollDie() {
// 	const randomNum = Math.floor(Math.random() * 6) + 1;
// 	console.log(randomNum);
// }

// function throwDice() {
// 	rollDie();
// 	rollDie();
// 	rollDie();
// }

// throwDice();

// function greet(firstName, lastName) {
// 	console.log(`Hello ${firstName} ${lastName}`);
// }

// greet('John', 'Doe');
// greet('Jane', 'Smith', 'Hello', 'World');

// function greet() {
// 	console.log(arguments);
// 	console.log(`Hello ${arguments[0]} ${arguments[1]}`);
// }

// greet('John', 'Doe', 'hello', 'world', 10, 20);

// function rollDie() {
// 	const randomNum = Math.floor(Math.random() * 6) + 1;
// 	console.log(randomNum);
// }

// function throwDice(times) {
// 	for (let i = 0; i < times; i++) {
// 		rollDie();
// 	}
// }

// throwDice(10);

// function greet(firstName, lastName) {
// 	return `Hello my name is ${firstName} ${lastName}`;
// }

// const result = greet('Jane', 'Smith');

// console.log('The value of result is', result);

// function isNumber(value) {
// 	if (typeof value === 'number') {
// 		return true;
// 	}
// 	return false;
// }

// console.log(isNumber('100'));

// let firstName = 'John';

// function greet() {
// 	var firstName = 'Jane';
// 	console.log(firstName);
// }

// greet();
// // console.log(firstName);

// let firstName = 'John';

// if (true) {
// 	var firstName = 'Jane';
// 	console.log(firstName);
// }

// console.log(firstName);

// for (var i = 0; i < 10; i++) {
// 	console.log(i);
// }

// console.log(i);

// var hello = 'World';
// var hello = 'world';

// let fullName = 'Jack Roe';

function outer() {
	// let fullName = 'John Doe';

	function inner() {
		let fullName = 'Jane Smith';
		console.log(fullName);
	}

	console.log(fullName);

	inner();
}

outer();
