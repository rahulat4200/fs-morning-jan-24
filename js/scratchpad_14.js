// // // // // const movieReviews = [4.5, 5.0, 3.2, 2.1, 4.7, 3.8, 3.1, 3.9, 4.4];
// // // // // const highest = Math.max(...movieReviews);
// // // // // const lowest = Math.min(...movieReviews);

// // // // // let total = 0;
// // // // // movieReviews.forEach((rating) => (total += rating));
// // // // // const average = total / movieReviews.length;

// // // // // const info1 = { highest, lowest, average };
// // // // // console.log(info1);

// // // // const getReviewDetails = (arr) => {
// // // // 	const highest = Math.max(...arr);
// // // // 	const lowest = Math.min(...arr);
// // // // 	const total = arr.reduce((accumulator, nextVal) => accumulator + nextVal);
// // // // 	const average = total / arr.length;

// // // // 	return {
// // // // 		highest,
// // // // 		lowest,
// // // // 		total,
// // // // 		average,
// // // // 	};
// // // // };

// // // // const reviewList = [4.5, 5.0, 3.2, 2.1, 4.7, 3.8, 3.1, 3.9, 4.4];

// // // // const statistics = getReviewDetails(reviewList);
// // // // console.log(statistics);

// // // const username = 'janedoe';
// // // const role = 'admin';

// // // const user1 = { [role]: username };
// // // console.log(user1);

// // // const addProperty = (obj, k, v) => {
// // // 	return { ...obj, [k]: v };
// // // };

// // // console.log(addProperty({ firstName: 'John' }, 'lastname', 'Doe'));

// // const math = {
// // 	PI: 3.1415,
// // 	add(a, b) {
// // 		return a + b;
// // 	},
// // 	sub(a, b) {
// // 		return a - b;
// // 	},
// // 	mul(a, b) {
// // 		return a * b;
// // 	},
// // 	div(a, b) {
// // 		return a / b;
// // 	},
// // };

// // console.log(math.add(10, 20));

// // function greet() {
// // 	console.log(this);
// // }

// // greet();
// // window.greet();

// // function greet() {
// // 	console.log(this);
// // }

// // const user = {
// // 	firstName: 'John',
// // 	lastName: 'Doe',
// // 	age: 20,
// // 	greet() {
// // 		console.log('Hello World');
// // 		console.log(this.firstName);
// // 	},
// // };

// // greet();
// // user.greet();

// // function greet() {
// // 	console.log(
// // 		`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old.`
// // 	);
// // }

// // // console.log(this.alert('Hello World'));

// // const user1 = {
// // 	firstName: 'Jane',
// // 	lastName: 'Doe',
// // 	age: 20,
// // 	greet,
// // };

// // const user2 = {
// // 	firstName: 'Richard',
// // 	lastName: 'Roe',
// // 	age: 25,
// // 	greet,
// // };

// // user1.greet();
// // user2.greet();

// // function aaaaaaaaaaaaaa() {
// // 	console.log('Hello World');
// // }

// // let aaaaaaaaaahello = 'Hello World';

// // console.log(this);

// // const user = {
// // 	firstName: 'John',
// // 	lastName: 'Doe',
// // 	role: 'admin',
// // 	fullName() {
// // 		return `${this.firstName} ${this.lastName} is an ${this.role}`;
// // 	},
// // 	logDetails() {
// // 		console.log(`${this.fullName()} and is cool!`);
// // 	},
// // };

// // const logDetails = user.logDetails;

// // logDetails();

// // const user1 = {
// // 	firstName: 'Jane',
// // 	lastName: 'Doe',
// // 	age: 30,
// // 	greet() {
// // 		console.log(
// // 			`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old`
// // 		);
// // 	},
// // };

// function User(firstName, lastName, age) {
// 	return {
// 		firstName,
// 		lastName,
// 		age,
// 		greet() {
// 			console.log(
// 				`Hello my name is ${this.firstName} ${this.lastName} and I am ${this.age} years old`
// 			);
// 		},
// 	};
// }

// const user1 = User('John', 'Doe', 20);
// user1.greet();
// console.log(user1.age);

// const hellos = {
// 	messages: [
// 		'hello world',
// 		'hello universe',
// 		'hello darkness',
// 		'hello hello',
// 		'heylo',
// 	],
// 	pickMsg: function () {
// 		const index = Math.floor(Math.random() * this.messages.length);
// 		return this.messages[index];
// 	},
// 	start() {
// 		setInterval(() => {
// 			// this -> window
// 			console.log(this.pickMsg());
// 		}, 1000);
// 	},
// };

// hellos.start();

const user1 = {
	firstName: 'john',
	greet: function () {
		console.log(this);
		// console.log(this.firstName);
	},
};

user1.greet();
