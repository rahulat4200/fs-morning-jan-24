// for (let count = 0; count < 100000; count++) {
// 	console.log(count, 'Hello World');
// }

// for (let count = 0; count < 10; count++) {
// 	console.log(count, 'Hello World');
// }

// const nums = [12, 34, 56, 34, 78, 54, 23, 12];

// let total = 0;
// for (let index = 0; index < nums.length; index++) {
// 	// console.log(nums[index]);
// 	total += nums[index];
// }

// console.log(total);

// const movies = [
// 	{ movieName: 'Inception', rating: 3.8 },
// 	{ movieName: 'Avengers', rating: 3.4 },
// 	{ movieName: 'Iron Man', rating: 2.9 },
// ];

// for (let i = 0; i < movies.length; i++) {
// 	const movie = movies[i];
// 	console.log(`${movie.movieName} has a rating ${movie.rating}`);
// }

// let name = 'John Doe';

// let reversedName = '';
// for (let char = name.length - 1; char >= 0; char--) {
// 	// console.log(name[char]);
// 	reversedName += name[char];
// }

// console.log(reversedName);

// for (let i = 0; i < 5; i++) {
// 	console.log(`${i} - Outer Loop`);

// 	for (let j = 0; j < 5; j++) {
// 		console.log(`    ${j} - Inner Loop`);
// 	}
// }

// const gameBoard = [
// 	[4, 64, 8, 4],
// 	[128, 32, 4, 16],
// 	[16, 4, 4, 32],
// 	[2, 16, 16, 2],
// ];

// let total = 0;

// for (let i = 0; i < gameBoard.length; i++) {
// 	// console.log(gameBoard[i]);

// 	for (let j = 0; j < gameBoard[i].length; j++) {
// 		// console.log(gameBoard[i][j]);
// 		total += gameBoard[i][j];
// 	}
// }

// console.log(total);

// const gameBoard = [
// 	[
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 	],
// 	[
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 	],
// 	[
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 	],
// 	[
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 		[4, 64, 8, 4],
// 	],
// ];

// for (let i = 0; i < 10; i++) {
// 	console.log(i);
// }

// let i = 0;
// while (i < 10) {
// 	console.log(i);
// 	i++;
// }

let target = Math.floor(Math.random() * 100) + 1;
let guess = Math.floor(Math.random() * 100) + 1;

while (true) {
	console.log(`TARGET: ${target} | GUESS: ${guess}`);
	guess = Math.floor(Math.random() * 100) + 1;
}

console.log(`FINAL RESULT:\nTARGET: ${target} | GUESS: ${guess}`);
