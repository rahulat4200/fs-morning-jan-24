// const h1 = document.querySelector('h1');
// h1.style.color = 'red';
// h1.style.backgroundColor = 'yellow';

// // const lis = document.querySelectorAll('ul li');
// const lis = document.all;
// let colors = ['red', 'yellow', 'green', 'blue', 'orange', 'purple'];
// let sizes = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

// for (let li of lis) {
// 	setInterval(() => {
// 		let randomColorNum1 = Math.floor(Math.random() * colors.length);
// 		let randomColorNum2 = Math.floor(Math.random() * colors.length);
// 		let randomColor1 = colors[randomColorNum1];
// 		let randomColor2 = colors[randomColorNum2];
// 		document.body.style.backgroundColor = randomColor1;
// 		li.style.color = randomColor2;
// 		let randonSizeNum = Math.floor(Math.random() * sizes.length);
// 		li.style.fontSize = `${sizes[randonSizeNum]}px`;
// 	}, 50);
// }

// let li = document.querySelector('li');
// // li.className = 'done';

// // console.log(li.classList);

// li.classList.add('done');
// li.classList.remove('hello');
// li.classList.toggle('world');

// const h1 = document.createElement('li');
// h1.innerText = 'Hello World, this is created using JavaScript.';
// h1.style.color = 'red';
// h1.className = 'title';
// // <h1 style='color: red;' class='title'>Hello World, this is created using JavaScript.</h1>

// // const root = document.querySelector('#root');
// const ul = document.querySelector('ul');
// const firstLi = document.querySelector('li');
// // ul.appendChild(h1);
// ul.insertBefore(h1, firstLi);

// const b = document.createElement('b');
// b.innerText = 'Bold text';
// const i = document.createElement('i');
// i.innerText = 'Hello World';

// const p = document.querySelector('p');
// // p.insertAdjacentElement('afterend', b);
// p.prepend(b);

// p.remove();

// function applyStyles(elRef, stylesObj) {
// 	for (let key in stylesObj) {
// 		elRef.style[key] = stylesObj[key];
// 	}
// }

// const heading = document.querySelector('h1');
// heading.innerText = 'Hello World';

// applyStyles(heading, {
// 	color: 'red',
// 	textDecoration: 'underline',
// 	backgroundColor: 'yellow',
// 	fontWeight: 500,
// });

// let colors = ['red', 'yellow', 'green', 'blue', 'orange', 'purple'];

// const btn = document.querySelector('button');
// // btn.onclick = function () {
// 	let randomNum = Math.floor(Math.random() * colors.length);
// 	document.body.style.backgroundColor = colors[randomNum];
// };
// btn.onclick = function () {
// 	console.log('Hello World');
// };

// btn.addEventListener('click', function () {
// 	let randomNum = Math.floor(Math.random() * colors.length);
// 	document.body.style.backgroundColor = colors[randomNum];
// });
// btn.addEventListener('mouseover', function () {
// 	console.log('Hello World');
// });
// window.addEventListener('scroll', function () {
// 	console.log('SCROLLL');
// });

const root = document.querySelector('#root');

const input = document.createElement('input');
input.type = 'text';
input.placeholder = 'Enter a task';

const btn = document.createElement('button');
btn.innerText = 'Add';

const container = document.createElement('div');
container.style.width = '850px';
container.style.margin = 'auto';
container.style.display = 'flex';
container.style.gap = '10px';
container.append(input, btn);

const taskList = document.createElement('ul');

btn.addEventListener('click', function () {
	if (!input.value) {
		return;
	}
	const li = document.createElement('li');
	li.innerText = input.value;
	li.style.cursor = 'pointer';
	li.style.listStyle = 'none';
	li.addEventListener('click', function () {
		li.classList.toggle('done');
	});

	const delBtn = document.createElement('button');
	delBtn.innerText = 'Delete';
	delBtn.addEventListener('click', function () {
		li.remove();
	});
	li.append(delBtn);
	taskList.append(li);
	input.value = '';
});

root.append(container);
root.append(taskList);
