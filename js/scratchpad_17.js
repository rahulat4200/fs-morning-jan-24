// function multiply(x, y) {
// 	return x * y;
// }

// function square(x) {
// 	return multiply(x, x);
// }

// function rightTriangle(x, y, z) {
// 	return square(x) + square(y) === square(z);
// }

// rightTriangle(1, 2, 3);

// console.log('First log');
// alert('Something ran');
// console.log('Second log');

// console.log('First log');
// setTimeout(() => {
// 	console.log('Hello World');
// }, 4000);
// console.log('Second log');

const btn = document.querySelector('button');
setTimeout(() => {
	btn.style.transform = `translateX(100px)`;
	setTimeout(() => {
		btn.style.transform = `translateX(200px)`;
		setTimeout(() => {
			btn.style.transform = `translateX(300px)`;
			setTimeout(() => {
				btn.style.transform = `translateX(400px)`;
				setTimeout(() => {
					btn.style.transform = `translateX(500px)`;
					setTimeout(() => {
						btn.style.transform = `translateX(600px)`;
					}, 1000);
				}, 1000);
			}, 1000);
		}, 1000);
	}, 1000);
}, 1000);
