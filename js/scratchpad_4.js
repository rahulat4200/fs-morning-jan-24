// let age = 10;

// if (age > 18) {
// 	console.log('Welcome');
// }

// let num = 25;

// if (num % 2 === 0) {
// 	console.log('Number is even!');
// }

// let age = 20;

// if (age >= 65) {
// 	console.log('Drinks are free');
// } else if (age >= 21) {
// 	console.log('You can enter and you can drink');
// } else if (age >= 18) {
// 	console.log('You can enter, but you cannot drink');
// } else {
// 	console.log("You can't enter!");
// }

// let age = 35;

// if (age >= 18) {
// 	console.log('You can enter, but you cannot drink');
// } else if (age >= 21) {
// 	console.log('You can enter and you can drink');
// } else if (age >= 65) {
// 	console.log('Drinks are free');
// } else {
// 	console.log("You can't enter!");
// }

// let password = 'helloworld@123';

// if (password.length >= 8) {
// 	if (password.search(' ') !== -1) {
// 		console.log('Password should not contain spaces');
// 	} else {
// 		console.log('Valid password');
// 	}
// } else {
// 	console.log('Password should be min 8 chars in length.');
// }

let loggedInUser = 'john';

if (loggedInUser !== null) {
	console.log('Welcome to my site');
} else {
	console.log('Please login to continue');
}
