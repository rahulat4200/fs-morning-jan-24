// function getData() {
// 	const response = axios.get('https://pokeapi.co/api/v2/pokemon');
// 	response.then((data) => {
// 		console.log(data);
// 	});
// }

// getData();

// async function greet() {
// 	throw 'Hello World!';
// }

// function greet() {
// 	return new Promise((resolve, reject) => {
// 		resolve('Hello World');
// 	});
// }

// greet()
// 	.then((data) => {
// 		console.log(data);
// 	})
// 	.catch((err) => {
// 		console.log(err);
// 	});

// async function add(x, y) {
// 	if (typeof x !== 'number' || typeof y !== 'number') {
// 		throw 'X and Y must be numbers';
// 	}
// 	return x + y;
// }

// add(10, '4').then((data) => console.log(data));

// function getData() {
// 	const response = axios.get('https://pokeapi.co/api/v2/pokemon');
// 	response.then((data) => {
// 		console.log(data.data.results);
// 	});
// }

// async function getData() {
// 	const response = await axios.get(
// 		'https://pokeapi.co/api/v2/poksadfsadfsadfasfdemon'
// 	);
// 	console.log(response);
// }

// getData().catch((err) => console.log(err.message));

// async function getData() {
// 	try {
// 		const response = await axios.get('https://pokeapi.co/api/v2/pokemon');
// 		console.log(response);
// 	} catch (err) {
// 		console.log('Something went wrong', err.message);
// 	}
// }

// getData();

// console.log('Hello');

// async function getData() {
// 	const pokemon1 = await axios.get('https://pokeapi.co/api/v2/pokemon/1');
// 	const pokemon2 = await axios.get('https://pokeapi.co/api/v2/pokemon/2');
// 	const pokemon3 = await axios.get('https://pokeapi.co/api/v2/pokemon/3');

// 	console.log(pokemon1.data);
// 	console.log(pokemon2.data);
// 	console.log(pokemon3.data);
// }

async function getData() {
	const pokemon1Promise = axios.get('https://pokeapi.co/api/v2/pokemon/1');
	const pokemon2Promise = axios.get('https://pokeapi.co/api/v2/pokemon/2');
	const pokemon3Promise = axios.get('https://pokeapi.co/api/v2/pokemon/3');

	const results = await Promise.all([
		pokemon1Promise,
		pokemon2Promise,
		pokemon3Promise,
	]);
	console.log(results);

	// const pokemon1 = await pokemon1Promise;
	// const pokemon2 = await pokemon2Promise;
	// const pokemon3 = await pokemon3Promise;

	// console.log(pokemon1.data);
	// console.log(pokemon2.data);
	// console.log(pokemon3.data);
}

getData();
